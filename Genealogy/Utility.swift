//
//  Utility.swift
//  Genealogy
//
//  Created by Evana Islam on 5/13/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    class func formatDate(_ date:Date, format:NSString)->NSString {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MMM dd, yyyy"
        return dateformatter.string(from: date) as NSString
    }


}
