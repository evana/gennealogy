//
//  HistoryListVC.swift
//  Genealogy
//
//  Created by Evana Islam on 5/13/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

import UIKit
import CoreData

class HistoryListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var histories : NSMutableArray!
    
    @IBOutlet weak var HistoryListTableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.histories = NSMutableArray()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchInfo() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FamilyHistory")
        
        do {
            let array = try context.fetch(fetchRequest) as NSArray!
            histories = array?.mutableCopy() as! NSMutableArray

            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
        }
        
        self.HistoryListTableView.reloadData()
        
    }
    //MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return histories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "history", for: indexPath) as! HistoryCell
        let familyHistory = self.histories.object(at: (indexPath as NSIndexPath).row) as! FamilyHistory
        cell.nameLbl.text = familyHistory.name
        cell.relationLbl.text = familyHistory.relation
        
        cell.dateLbl.text = Utility.formatDate(familyHistory.birthDay!, format: "MMM dd, yyyy") as String
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
