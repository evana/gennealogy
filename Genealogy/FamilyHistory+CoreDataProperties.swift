//
//  FamilyHistory+CoreDataProperties.swift
//  Genealogy
//
//  Created by Evana Islam on 5/13/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FamilyHistory {

    @NSManaged var name: String?
    @NSManaged var relation: String?
    @NSManaged var birthDay: Date?

}
