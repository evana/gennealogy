//
//  HistoryDetailVC.swift
//  Genealogy
//
//  Created by Evana Islam on 5/13/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

import UIKit
import CoreData

class HistoryDetailVC: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var relationTF: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatDate(Date())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func formatDate(_ date:Date) {
        
        self.dateLbl.text = Utility.formatDate(date, format: "MMM dd, yyyy") as String
    }
    
    @IBAction func showDateOnLabel(_ sender: AnyObject) {
        formatDate(datePicker.date)
    }

    @IBAction func saveInfo(_ sender: AnyObject) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        let familyHistory = NSEntityDescription.insertNewObject(forEntityName: "FamilyHistory", into: context) as! FamilyHistory
        
        familyHistory.name = nameTF.text
        familyHistory.relation = relationTF.text
        familyHistory.birthDay = datePicker.date
        
        if appDelegate.saveContext() {
            
            let alertController = UIAlertController(title: "Genealogy", message: "Information Successfully Saved.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            //Clear form
            self.nameTF.text = ""
            self.relationTF.text = ""
            self.datePicker.date = Date()
            self.formatDate(self.datePicker.date)
        }
    }
    
}
